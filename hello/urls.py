from django.urls import path,include
from .import views

urlpatterns=[
    path('hiii',views.say_hello, name=('say_hello')),
    path('bye',views.say_goodbye,name=('say_goodbye')),
    path('fibonacci',views.get_nth_fibonacci,name=('nth_fibonacci')),
    path('fib/<int:n>',views.get_nth_fibonacci_alt,name=('nth_fibonacci_alt')),
    path('students/all', views.student_list, name='student_list'),
    path('students/<int:roll_no_>', views.student_detail, name='student_detail'),
    path('students/add', views.update_or_create_student, name='create_student'),
    path('students/<int:id_>/edit', views.update_or_create_student, name='edit_student'),
    path('students/<int:roll_no_>/delete', views.delete_student, name='delete_student'),
    path('employees/all', views.employee_list, name='employee_list'),
    path('employees/<int:eid_>', views.employee_detail, name='employee_detail'),

]