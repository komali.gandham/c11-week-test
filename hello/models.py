from django.db import models

#create your views here


class Student (models.Model):
    name = models.CharField(max_length=25)
    roll_no = models.IntegerField()

    def __str__(self):
        return f'Student: {self.roll_no} : {self.name}'
    
class Employee(models.Model):
    eid=models.IntegerField()
    ename = models.CharField(max_length=25)
    location=models.CharField(max_length=20)
    salary=models.IntegerField()

    def _str_(self):
        return f'Employee: {self.eid} : {self.ename} : {self.location} : {self.salary}'
